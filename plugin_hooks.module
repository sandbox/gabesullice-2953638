<?php

/**
 * @file
 * Functions to shim hooks which aren't invoked by the module_handler service.
 */

/**
 * Calls a shimmed plugin hook implementation.
 *
 * @param string $module
 *   The module name with a shimmed plugin implementation.
 * @param string $shimmed
 *   The module hook function name.
 * @param array $args
 *   The invocation arguments.
 *
 * @return mixed
 *   The return value of the shimmed plugin.
 */
function plugin_hooks_shim_invoke($module, $shimmed, array $args = []) {
  return \Drupal::service('plugin.manager.plugin_hooks')->invoke(
    $module,
    substr($shimmed, strlen($module) + 1) . '_shimmed',
    $args
  );
}

/**
 * Calls a shimmed plugin hook implementation.
 *
 * @param string $module
 *   The module name with a shimmed plugin implementation.
 * @param string $shimmed
 *   The module hook function name.
 * @param mixed $data
 *   The data to be altered.
 * @param mixed $context1
 *   Alterable context.
 * @param mixed $context2
 *   Alterable context.
 */
function plugin_hooks_shim_alter($module, $shimmed, &$data, &$context1 = NULL, &$context2 = NULL) {
  \Drupal::service('plugin.manager.plugin_hooks')->moduleAlter(
    $module,
    substr($shimmed, strlen($module) + 1) . '_shimmed',
    $data, $context1, $context2
  );
}


