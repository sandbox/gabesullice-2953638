<?php

namespace Drupal\plugin_hooks\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface for managing hook plugins.
 *
 * @package Drupal\plugin_hooks\Plugin
 */
interface HookPluginManagerInterface extends PluginManagerInterface {

  /**
   * Determines which modules are implementing a hook.
   *
   * @inheritdoc \Drupal\Core\Extension\ModuleHandlerInterface::getImplementations()
   */
  public function getImplementations($hook);

  /**
   * Returns whether a given module implements a given hook.
   *
   * @inheritdoc \Drupal\Core\Extension\ModuleHandlerInterface::alter()
   */
  public function implementsHook($module, $hook);

  /**
   * Invoke alter hook plugins.
   *
   * @inheritdoc \Drupal\Core\Extension\ModuleHandlerInterface::alter()
   */
  public function alter($type, &$data, &$context1 = NULL, &$context2 = NULL);

  /**
   * Invoke a hook plugin.
   *
   * @inheritdoc \Drupal\Core\Extension\ModuleHandlerInterface::invoke()
   */
  public function invoke($module, $hook, array $args = []);

  /**
   * Invoke all hook plugins for hook.
   *
   * @inheritdoc \Drupal\Core\Extension\ModuleHandlerInterface::invokeAll()
   */
  public function invokeAll($hook, array $args = []);

}