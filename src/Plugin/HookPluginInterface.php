<?php

namespace Drupal\plugin_hooks\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Hook plugin plugins.
 */
interface HookPluginInterface extends PluginInspectionInterface {

  /**
   * Reset any class properties that were set during invocation.
   *
   * By default, plugin instances are statically cached so that they do not need
   * to be reinstantiated for every invocation. However, if the plugin sets any
   * properties that cannot be shared across invocations, this method should
   * unset those values. This method will be called immediately after the hook
   * method is called.
   */
  public function reset();

}
