<?php

namespace Drupal\plugin_hooks\Plugin\Hooks;

use Drupal\plugin_hooks\Plugin\HookPluginBase;

/**
 * Implements hook_help().
 *
 * @Hook(id = "plugin_hooks_help")
 */
class Help extends HookPluginBase {

  /**
   * Provides help text for the JSON API module.
   *
   * @inheritdoc hook_help()
   */
  public function hook($route_name) {
    if ($route_name === 'help.page.plugin_hooks') {
      $lines = [
        $this->t('<h2>Plugin Hooks</h2>'),
        $this->t("<p>The Plugin Hooks module makes it possible to implement all of Drupal's hooks as plugins.</p>"),
        $this->t('<p>In doing so, it becomes possible to create any Drupal module in a completely object-oriented fashion.&nbsp;'),
        $this->t('You can inject services from the container or use <code>$this->t()</code> as you would with any other class.</p>'),
        $this->t("To keep your code easy to read, you can now easily create real <code>protected</code> helper methods that can't be called from outside your hook too!</p>"),
        $this->t('<h3>Quick Start</h3>'),
        $this->t("<pre>"),
        $this->t("@code\n", ['@code' => '<?php']),
        $this->t("@code\n", ['@code' => '']),
        $this->t("@code\n", ['@code' => 'namespace Drupal\your_module_name_here\Plugin\Hooks;']),
        $this->t("@code\n", ['@code' => '']),
        $this->t("@code\n", ['@code' => 'use Drupal\plugin_hooks\Plugin\HookPluginBase;']),
        $this->t("@code\n", ['@code' => '']),
        $this->t("@code\n", ['@code' => '/**']),
        $this->t("@code\n", ['@code' => ' * Implements hook_help().']),
        $this->t("@code\n", ['@code' => ' *']),
        $this->t("@code\n", ['@code' => ' * @Hook(id = "your_module_name_here_help")']),
        $this->t("@code\n", ['@code' => ' */']),
        $this->t("@code\n", ['@code' => 'class Help extends HookPluginBase {']),
        $this->t("@code\n", ['@code' => '']),
        $this->t("@code\n", ['@code' => '  public function hook($route_name, RouteMatchInterface $route_match) {']),
        $this->t("@code\n", ['@code' => '    // Implement your hook here.']),
        $this->t("@code\n", ['@code' => '  }']),
        $this->t("@code\n", ['@code' => '']),
        $this->t("@code\n", ['@code' => '}']),
        $this->t("</pre>"),
        $this->t('<h3>FAQ</h3>'),
        $this->t('<h5>Can I add this module to an exisiting site?</h5>'),
        $this->t('<p>Absolutely, all your old hooks will continue to work as expected. You can choose to only&nbsp;'),
        $this->t('implement new hooks with plugins or progressively migrate them.</p>'),
        $this->t('<h5>My plugin is never called. Why?</h5>'),
        $this->t('<p>Some modules do not use the <code>module_handler</code> service to invoke hooks, this is a <a href="https://www.drupal.org/project/drupal/issues/2616814">known issue.</a></p>'),
        $this->t('<p>For these cases, this module provides a two "shim" functions to work around that issue. Simply implement the procedural style hook in your .module file.&nbsp;'),
        $this->t('as you normally would. Then, call either <code>plugin_hooks_shim_alter()</code> for alter hooks or <code>plugin_hooks_shim_invoke()for all others</code>.</p>'),
        $this->t('For example, to implement <code>hook_cron</code> you would write this in your <code>.module</code> file:</p>'),
        $this->t("<pre>"),
        $this->t("@code\n", ['@code' => 'function mymodule_cron() {']),
        $this->t("@code\n", ['@code' => "  plugin_hooks_shim_invoke('mymodule', __FUNCTION__, func_get_args());"]),
        $this->t("@code\n", ['@code' => '}']),
        $this->t("</pre>"),
        $this->t("<p>Then, you're plugin ID should be <code>mymodule_cron_shimmed</code>.</p>"),
        $this->t('<h5>Can I still use the "old style" hooks if I install this module?</h5>'),
        $this->t('<p>Yes, of course! You just may not implement the <em>same</em> hook procedurally <em>and</em> as a plugin hook in the <em>same</em> module.</p>'),
        $this->t('<h3>Tips &amp; Advanced Usage</h3>'),
        $this->t('<h5>Override the hook method name</h5>'),
        $this->t('<p>By default, your hook plugin method must be named <code>hook</code>.&nbsp;'),
        $this->t('However, it is quite easy to customize.</p>'),
        $this->t('<p>Simply add <code>method = "yourCustomMethodName"</code> to the annotation. Like so:</p>'),
        $this->t("<pre>"),
        $this->t("@code\n", ['@code' => '/**']),
        $this->t("@code\n", ['@code' => ' * @Hook(']),
        $this->t("@code\n", ['@code' => ' *   id = "your_module_name_here_help"']),
        $this->t("@code\n", ['@code' => ' *   method = "yourCustomMethodName"']),
        $this->t("@code\n", ['@code' => ' * )']),
        $this->t("@code\n", ['@code' => ' */']),
        $this->t("</pre>"),
        $this->t('<h5>Inject services from the container</h5>'),
        $this->t("<pre>"),
        $this->t("@code\n", ['@code' => 'use Drupal\plugin_hooks\Plugin\HookPluginBase;']),
        $this->t("@code\n", ['@code' => 'use Drupal\Core\Entity\EntityTypeManagerInterface;']),
        $this->t("@code\n", ['@code' => 'use Symfony\Component\DependencyInjection\ContainerInterface;']),
        $this->t("@code\n", ['@code' => '']),
        $this->t("@code\n", ['@code' => 'class Help extends HookPluginBase {']),
        $this->t("@code\n", ['@code' => '']),
        $this->t("@code\n", ['@code' => '  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {']),
        $this->t("@code\n", ['@code' => '    parent::__construct(array $configuration, $plugin_id, $plugin_definition);']),
        $this->t("@code\n", ['@code' => '    $this->entityTypeManager = $entity_type_manager;']),
        $this->t("@code\n", ['@code' => '  }']),
        $this->t("@code\n", ['@code' => '']),
        $this->t("@code\n", ['@code' => '  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {']),
        $this->t("@code\n", ['@code' => '    return new static($configuration, $plugin_id, $plugin_definition, $container->get(\'entity_type.manager\'));']),
        $this->t("@code\n", ['@code' => '  }']),
        $this->t("@code\n", ['@code' => '']),
        $this->t("@code\n", ['@code' => '}']),
        $this->t("</pre>"),
        $this->t('<h3>Gotchas</h3>'),
        $this->t('<p>By default, plugin instances are statically cached so that they do not need to be&nbsp;'),
        $this->t('reinstantiated for every invocation. That means that if the plugin sets any properties&nbsp;'),
        $this->t('that cannot be shared across invocations, they need to be unset before the hook is called again.</p>'),
        $this->t('<p>To do that, implement <code>public function reset();</code> in your plugin and unset&nbsp;'),
        $this->t("any properties that can't be reused. The <code>reset()</code> method will be called immediately&nbsp;"),
        $this->t("after the hook's method is called.</p>"),
      ];
      return array_map(function ($line) {
        return ['#markup' => $line];
      }, $lines);
    }
    return NULL;
  }

}

