<?php

namespace Drupal\plugin_hooks\Plugin;

use Drupal\Component\Assertion\Inspector;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Hook plugin plugin manager.
 *
 * @internal
 */
class HookPluginManager extends DefaultPluginManager implements HookPluginManagerInterface {

  /**
   * An array of plugin definitions, keyed by hook name.
   *
   * @var array
   */
  protected $implementations = [];

  /**
   * An array of alter hook plugin definitions.
   *
   * @var array
   */
  protected $alters = [];

  /**
   * An array of hook plugin instances.
   *
   * @var \Drupal\plugin_hooks\Plugin\HookPluginInterface[]
   */
  protected $plugins = [];

  /**
   * Constructs a new HookPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Hooks', $namespaces, $module_handler, 'Drupal\plugin_hooks\Plugin\HookPluginInterface', 'Drupal\plugin_hooks\Annotation\Hook');
    $this->alterInfo(FALSE);
    $this->setCacheBackend($cache_backend, 'plugin_hooks_hook_plugin_plugins');
  }

  /**
   * Determines which plugins are implementing a hook.
   *
   * @param string $hook
   *   The hook name.
   *
   * @return array
   *   An array of plugin definitions, keyed by plugin ID, which implement the
   *   given hook.
   */
  protected function getImplementing($hook) {
    if (!isset($this->implementations[$hook])) {
      $implementations = array_filter($this->getDefinitions(), function ($plugin_definition) use ($hook) {
        return $plugin_definition['hook'] === $hook;
      });
      $this->implementations[$hook] = array_combine(array_column($implementations, 'provider'), array_values($implementations));
    }
    return $this->implementations[$hook];
  }

  /**
   * {@inheritdoc}
   */
  public function getImplementations($hook) {
    return array_keys($this->getImplementing($hook));
  }

  /**
   * {@inheritdoc}
   */
  public function implementsHook($module, $hook) {
    return in_array($module, $this->getImplementations($hook));
  }

  /**
   * Builds a static cache of plugin definitions implementing $type.
   *
   * @param string|string[] $type
   *   A string or array of hook type names.
   *
   * @return array
   *   An array of alter implementations, keyed by module name.
   */
  protected function getAlterImplementations($type) {
    $cid = is_array($type) ? implode(',', $type) : $type;
    if (!isset($this->alters[$cid])) {
      $this->alters[$cid] = array_reduce(is_array($type) ? $type : [$type], function ($alters, $type) {
        $implementations = $this->getImplementing($type . '_alter');
        return array_reduce($implementations, function ($alters, $implementation) use ($type) {
          $alters[$implementation['provider']][$type] = $implementation;
          return $alters;
        }, $alters);
      }, []);
    }
    return $this->alters[$cid];
  }

  /**
   * {@inheritdoc}
   */
  public function alter($type, &$data, &$context1 = NULL, &$context2 = NULL) {
    foreach ($this->getAlterImplementations($type) as $module => $implementations) {
      foreach ($implementations as $implementation) {
        $this->pluginAlter($implementation, $data, $context1, $context2);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function invoke($module, $hook, array $args = []) {
    $implementations = $this->getImplementing($hook);
    return isset($implementations[$module]) ? $this->pluginInvoke($implementations[$module], $args) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function invokeAll($hook, array $args = []) {
    $return = [];
    foreach ($this->getImplementing($hook) as $plugin_definition) {
      $return = array_merge_recursive($return, $this->pluginInvoke($plugin_definition, $args));
    }
    return $return;
  }

  /**
   * Invokes a single plugin hook alter.
   *
   * Do not call this method. This class is for internal use only.
   *
   * @param string $module
   *   The module for which the alter should be invoked.
   * @param string $type
   *   The alter type name. Notably, only a string is accepted, not an array.
   * @param mixed $data
   *   The data to be altered.
   * @param mixed $context1
   *   Alterable context.
   * @param mixed $context2
   *   Alterable context.
   */
  public function moduleAlter($module, $type, &$data, &$context1 = NULL, &$context2 = NULL) {
    $implementations = $this->getAlterImplementations($type);
    if (isset($implementations[$module][$type])) {
      $this->pluginAlter($implementations[$module][$type], $data, $context1, $context2);
    }
    else {
      throw new \InvalidArgumentException("$module does not implement $type.");
    }
  }

  /**
   * Invoke a hook alter plugin instance.
   *
   * @param array $plugin_definition
   *   The plugin definition.
   * @param mixed $data
   *   The variable that will be passed to implementation to be altered.
   * @param mixed $context1
   *   (optional) An additional variable that is passed by reference.
   * @param mixed $context2
   *   (optional) An additional variable that is passed by reference. If more
   *   context needs to be provided to implementations, then this should be an
   *   associative array as described above.
   *
   * @see \Drupal\Core\Extension\ModuleHandlerInterface::alter()
   */
  protected function pluginAlter(array $plugin_definition, &$data, &$context1 = NULL, &$context2 = NULL) {
    $instance = $this->pluginInstance($plugin_definition);
    $instance->{$plugin_definition['method']}($data, $context1, $context2);
    $instance->reset();
  }

  /**
   * Invoke a hook plugin instance.
   *
   * @param array $plugin_definition
   *   The plugin definition.
   * @param array $args
   *   Arguments to pass to the hook implementation.
   *
   * @return mixed
   *   The return value of the hook implementation.
   */
  protected function pluginInvoke(array $plugin_definition, array $args = []) {
    $instance = $this->pluginInstance($plugin_definition);
    $return = $instance->{$plugin_definition['method']}(...$args);
    $instance->reset();
    return $return;
  }

  /**
   * Gets a hook plugin instance.
   *
   * @param array $plugin_definition
   *   The plugin definition.
   *
   * @return \Drupal\plugin_hooks\Plugin\HookPluginInterface
   *   The hook plugin instance.
   */
  protected function pluginInstance(array $plugin_definition) {
    if (!$plugin_definition['static'] || !isset($this->plugins[$plugin_definition['id']])) {
      /* @var \Drupal\plugin_hooks\Plugin\HookPluginInterface $instance */
      $instance = $this->createInstance($plugin_definition['id']);
      if (!$plugin_definition['static']) {
        return $instance;
      }
      else {
        $this->plugins[$plugin_definition['id']] = $instance;
      }
    }
    return $this->plugins[$plugin_definition['id']];
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);
    $definition['hook'] = substr($plugin_id, strlen($definition['provider']) + 1);
    $definition['category'] = $definition['provider'];
    $plugin_class = $this->getFactory()->getPluginClass($plugin_id, $definition);
    $plugin_method = $definition['method'];
    assert(method_exists($plugin_class, $plugin_method), "$plugin_class does not implement the '$plugin_method' method.");
  }

  /**
   * {@inheritdoc}
   */
  public function clearCachedDefinitions() {
    parent::clearCachedDefinitions();
    $this->implementations = [];
    $this->alters = [];
    $this->plugins = [];
  }

}
