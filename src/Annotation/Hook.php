<?php

namespace Drupal\plugin_hooks\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Hook plugin item annotation object.
 *
 * @see \Drupal\plugin_hooks\Plugin\HookPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class Hook extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The method to call when the hook is invoked.
   *
   * @var string
   */
  public $method = 'hook';

  /**
   * Whether the hook plugin can be statically cached between invocations.
   *
   * By default, plugin instances are statically cached so that they do not need
   * to be reinstantiated for every invocation. However, if the plugin relies on
   * injected services which may change during the lifetime of the request, this
   * can be set to TRUE so that a new plugin instance is created before each
   * invocation. This incurs a performance penalty if the plugin will be invoked
   * repeatedly.
   *
   * @var bool
   */
  public $static = TRUE;

}
