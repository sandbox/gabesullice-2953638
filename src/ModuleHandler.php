<?php

namespace Drupal\plugin_hooks;

use Drupal\Component\Assertion\Inspector;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandler as CoreModuleHandler;
use Drupal\plugin_hooks\Plugin\HookPluginManagerInterface;

/**
 * Extends the core module handler to invoke hook plugins.
 *
 * @package Drupal\plugin_hooks
 */
class ModuleHandler extends CoreModuleHandler {

  /**
   * The hook plugin manager.
   *
   * @var \Drupal\plugin_hooks\Plugin\HookPluginManagerInterface|\Drupal\Component\Plugin\Discovery\CachedDiscoveryInterface
   */
  protected $pluginHookManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(string $root, array $module_list, CacheBackendInterface $cache_backend, HookPluginManagerInterface $hook_plugin_manager) {
    parent::__construct($root, $module_list, $cache_backend);
    $this->pluginHookManager = $hook_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getImplementations($hook) {
    $implementations = array_keys(array_flip(array_merge(
      parent::getImplementations($hook),
      $this->pluginHookManager->getImplementations($hook)
    )));
    assert(Inspector::assertAll(function ($module) use ($hook) {
      return $this->assertOneImplementation($module, $hook);
    }, $implementations), "A module must not implement the $hook hook as a plugin and in its module file.");
    return $implementations;
  }

  /**
   * {@inheritdoc}
   */
  public function implementsHook($module, $hook) {
    return parent::implementsHook($module, $hook) || $this->pluginHookManager->implementsHook($module, $hook);
  }

  /**
   * Invoke alter hook plugins.
   *
   * @inheritdoc \Drupal\Core\Extension\ModuleHandlerInterface::alter()
   */
  public function alter($type, &$data, &$context1 = NULL, &$context2 = NULL) {
    parent::alter($type, $data, $context1, $context2);
    $this->pluginHookManager->alter($type, $data, $context1, $context2);
  }

  /**
   * Invoke a hook plugin.
   *
   * @inheritdoc \Drupal\Core\Extension\ModuleHandlerInterface::invoke()
   */
  public function invoke($module, $hook, array $args = []) {
    if (parent::implementsHook($module, $hook)) {
      return parent::invoke($module, $hook, $args) ?: $this->pluginHookManager->invoke($module, $hook, $args);
    }
    elseif ($this->pluginHookManager->implementsHook($module, $hook)) {
      return $this->pluginHookManager->invoke($module, $hook, $args);
    }
    else {
      return NULL;
    }
  }

  /**
   * Invoke all hook plugins for hook.
   *
   * @inheritdoc \Drupal\Core\Extension\ModuleHandlerInterface::invokeAll()
   */
  public function invokeAll($hook, array $args = []) {
    return array_merge_recursive(
      parent::invokeAll($hook, $args),
      $this->pluginHookManager->invokeAll($hook, $args)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function resetImplementations() {
    parent::resetImplementations();
    $this->pluginHookManager->clearCachedDefinitions();
  }

  /**
   * Whether less than two hook implementations exist for a module.
   *
   * @param string $module
   *   A module name.
   * @param string $hook
   *   A hook name.
   *
   * @return bool
   *   TRUE if the hook is not implemented or is only implemented once, FALSE
   *   otherwise.
   */
  public function assertOneImplementation($module, $hook) {
    $implements_procedural = parent::implementsHook($module, $hook);
    $implements_plugin = $this->pluginHookManager->implementsHook($module, $hook);
    return !($implements_procedural && $implements_plugin);
  }

}